//
//  DevicesProviderStub.swift
//  HomieTests
//
//  Created by Renxiao Mo on 03/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxCocoa
@testable import Homie

class DevicesProviderStub: DevicesProvider {
    var devices: BehaviorRelay<[Device]> {
        let devices = [DeviceObject(id: 1,
                                    deviceName: "Lampe - Cuisine",
                                    productType: "Light",
                                    intensity: 50,
                                    temperature: nil,
                                    mode: "ON",
                                    position: nil),
                       DeviceObject(id: 2,
                                    deviceName: "Radiateur - Chambre",
                                    productType: "Heater",
                                    intensity: nil,
                                    temperature: 20,
                                    mode: "OFF",
                                    position: nil),
                       DeviceObject(id: 3,
                                    deviceName: "Volet roulant - Salon",
                                    productType: "RollerShutter",
                                    intensity: nil,
                                    temperature: nil,
                                    mode: nil,
                                    position: 70),
        ]
        
        return BehaviorRelay(value: devices.compactMap { $0.toDevice })
    }
}
