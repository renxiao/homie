//
//  FilterProviderStub.swift
//  HomieTests
//
//  Created by Renxiao Mo on 03/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxCocoa

@testable import Homie

class FilterProviderStub: FilterProvider {
    
    var filters = BehaviorRelay<[Filter]>(value: [
        Filter(type: .light, isEnabled: true),
        Filter(type: .heater, isEnabled: true),
        Filter(type: .rollerShutter, isEnabled: true)
    ])
}
