//
//  HomieTests.swift
//  HomieTests
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import XCTest
import RxSwift
import RxCocoa
import RxBlocking
import RxTest

@testable import Homie

class HomieTests: XCTestCase {

    var scheduler: TestScheduler!
    var disposeBag: DisposeBag!
    var devicesProviderStub: DevicesProviderStub!
    
    override func setUpWithError() throws {
        scheduler = TestScheduler(initialClock: 0)
        disposeBag = DisposeBag()
        devicesProviderStub = DevicesProviderStub()
    }

    override func tearDownWithError() throws {
        scheduler = nil
        disposeBag = nil
        devicesProviderStub = nil
    }
    
    func test__devices_are_create_correctly_with_dedicated_class() {

        let sut = DevicesViewModel(devicesProvider: devicesProviderStub)
        let devices = sut.devices.value

        XCTAssertEqual(devices.count, 3)
        XCTAssertEqual(devices[0] is Light, true)
        XCTAssertEqual(devices[1] is Heater, true)
        XCTAssertEqual(devices[2] is RollerShutter, true)
    }
    
    func test__light_detail_information_is_correct() {
        let stub = DevicesProviderStub()

        guard let light = stub.devices.value.first as? Light else {
            XCTAssert(false, "Expected device to be a light")
            return
        }
        
        let sut = DeviceDetailViewModel(device: light, devicesProvider: stub)
        let expectedSliderValue = Float(light.intensity) / 100
        
        XCTAssertEqual(sut.title, "Lampe - Cuisine")
        XCTAssertEqual(sut.sliderType, .normal)
        XCTAssertEqual(sut.isOnOffButtonHidden, false)
        XCTAssertEqual(try sut.isOn.toBlocking().first(), true)
        XCTAssertEqual(try sut.textValue.toBlocking().first(), "50")
        XCTAssertEqual(try sut.sliderValue.toBlocking().first(), expectedSliderValue)
    }
    
    func test__heater_detail_information_is_correct() {
        guard let heater = devicesProviderStub.devices.value[1] as? Heater else {
            XCTAssert(false, "Expected device to be a heater")
            return
        }
        
        let sut = DeviceDetailViewModel(device: heater, devicesProvider: devicesProviderStub)
        
        let stespByDegree = 2
        let minDegree = 7
        let expectedSliderValue = Float((heater.temperature - minDegree) * stespByDegree) / 100
        
        XCTAssertEqual(sut.title, "Radiateur - Chambre")
        XCTAssertEqual(sut.sliderType, .heater)
        XCTAssertEqual(sut.isOnOffButtonHidden, false)
        XCTAssertEqual(try sut.isOn.toBlocking().first(), false)
        XCTAssertEqual(try sut.textValue.toBlocking().first(), "20")
        XCTAssertEqual(try sut.sliderValue.toBlocking().first(), expectedSliderValue)
    }
    
    func test__roller_shutter_detail_information_is_correct() {
        guard let rollerShutter = devicesProviderStub.devices.value[2] as? RollerShutter else {
            XCTAssert(false, "Expected device to be a roller shutter")
            return
        }
        
        let sut = DeviceDetailViewModel(device: rollerShutter, devicesProvider: devicesProviderStub)
        let expectedSliderValue = Float(rollerShutter.position) / 100
        
        XCTAssertEqual(sut.title, "Volet roulant - Salon")
        XCTAssertEqual(sut.sliderType, .normal)
        XCTAssertEqual(sut.isOnOffButtonHidden, true)
        XCTAssertEqual(try sut.isOn.toBlocking().first(), false)
        XCTAssertEqual(try sut.textValue.toBlocking().first(), "70")
        XCTAssertEqual(try sut.sliderValue.toBlocking().first(), expectedSliderValue)
    }
    
    func test__when_changing_heater_slider_value__temperature_is_displaying_correctly() {
        guard let heater = devicesProviderStub.devices.value[1] as? Heater else {
            XCTAssert(false, "Expected device to be a heater")
            return
        }
        
        let sut = DeviceDetailViewModel(device: heater, devicesProvider: devicesProviderStub)
        let stespByDegree = 2
        let minDegree = 7
        let maxDegree = 28
        let expectedSliderValue = Float((heater.temperature - minDegree) * stespByDegree) / 100

        // initial state
        XCTAssertEqual(try sut.textValue.toBlocking().first(), "20")
        XCTAssertEqual(try sut.sliderValue.toBlocking().first(), expectedSliderValue)

        // when slider value is at at minimum
        let minimumSliderValue = Float(0)
        let sliderValue = scheduler.createObserver(Float.self)
        sut.sliderValueBehaviorRelay.bind(to: sliderValue).disposed(by: disposeBag)
        sut.sliderValueBehaviorRelay.accept(minimumSliderValue)
        var expectedTemperatureText = "7"
        
        XCTAssertEqual(try sut.textValue.toBlocking().first(), expectedTemperatureText)

        // when slider value is at maximum
        
        let maxSliderValue = Float((maxDegree - minDegree) * stespByDegree) / 100
        sut.sliderValueBehaviorRelay.bind(to: sliderValue).disposed(by: disposeBag)
        sut.sliderValueBehaviorRelay.accept(maxSliderValue)
        expectedTemperatureText = "28"
        
        XCTAssertEqual(try sut.textValue.toBlocking().first(), expectedTemperatureText)
    }
    
    func test__filter_devices() {
        let filterProviderStub = FilterProviderStub()
        let sut = DevicesViewModel(devicesProvider: devicesProviderStub, filterProvider: filterProviderStub)
        
        XCTAssertEqual(sut.shouldShowDeviceType(.light), true)
        XCTAssertEqual(sut.shouldShowDeviceType(.heater), true)
        XCTAssertEqual(sut.shouldShowDeviceType(.rollerShutter), true)
        
        // when
        var filters = filterProviderStub.filters.value
        filters[0].isEnabled = false
        filters[1].isEnabled = false
        filters[2].isEnabled = false
        filterProviderStub.filters.accept(filters)
        
        XCTAssertEqual(sut.shouldShowDeviceType(.light), false)
        XCTAssertEqual(sut.shouldShowDeviceType(.heater), false)
        XCTAssertEqual(sut.shouldShowDeviceType(.rollerShutter), false)
    }

    func test__profile_edit_form_validation() {
        let user = User(firstName: "John", lastName: "Doe", address: User.UserAddress(city: "Issy-les-Moulineaux", postalCode: 92130, street: "rue Michelet", streetCode: "2B", country: "France"), birthDate: 813766371000)
        let sut = EditUserProfileViewModel(user: user)
        
        XCTAssertEqual(try sut.isBirthdayValid.toBlocking().first(), true)

        sut.birthdayDate.accept("")
        XCTAssertEqual(try sut.isBirthdayValid.toBlocking().first(), false)
        
        sut.birthdayDate.accept("01122000")
        XCTAssertEqual(try sut.isBirthdayValid.toBlocking().first(), false)

        sut.birthdayDate.accept("01/12/00")
        XCTAssertEqual(try sut.isBirthdayValid.toBlocking().first(), false)
        
        sut.birthdayDate.accept("01/12/2000")
        XCTAssertEqual(try sut.isBirthdayValid.toBlocking().first(), true)
    }
}
