//
//  DateFormatter+Extension.swift
//  Homie
//
//  Created by Renxiao Mo on 02/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import Foundation

extension DateFormatter {
    static var birthDateFormatter: DateFormatter {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        
        return formatter
    }
}
