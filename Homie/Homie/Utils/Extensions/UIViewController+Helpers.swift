//
//  UIViewController+Helpers.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

extension UIViewController {
    func setupFilterButtonInNavBar(filterSelector: Selector, editSelector: Selector) {
        let filterImage = UIImage(named: "filter")?.withRenderingMode(.alwaysTemplate)
        let editImage = UIImage(named: "edit")?.withRenderingMode(.alwaysTemplate)

        let filterItem = UIBarButtonItem(image: filterImage, style: .plain, target: self, action: filterSelector)
        let editItem = UIBarButtonItem(image: editImage, style: .plain, target: self, action: editSelector)

        navigationItem.rightBarButtonItems = [editItem, filterItem]
    }
    
    func setupCancelButtonInNavBar(selector: Selector) {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "cancel".localized, style: .plain, target: self, action: selector)
    }
    
    @discardableResult
    func setupDoneButtonInNavBar(selector: Selector) -> UIBarButtonItem {
        let buttonItem = UIBarButtonItem(title: "save".localized, style: .done, target: self, action: selector)
        navigationItem.rightBarButtonItem = buttonItem
        
        return buttonItem
    }
    
    func setupCloseButtonInNavBar(selector: Selector) {
        let buttonItem = UIBarButtonItem(title: "close".localized, style: .done, target: self, action: selector)
        navigationItem.rightBarButtonItem = buttonItem
    }
    
    func setupEditButtonInNavBar(selector: Selector) {
        let image = UIImage(named: "edit")?.withRenderingMode(.alwaysTemplate)
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: image, style: .plain, target: self, action: selector)
    }
}
