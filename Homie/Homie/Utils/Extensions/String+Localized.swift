//
//  String+Localized.swift
//  Homie
//
//  Created by Renxiao Mo on 02/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
