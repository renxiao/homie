//
//  UIColor+Extension.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(rgb: UInt) {
       self.init(red: CGFloat((rgb & 0xFF0000) >> 16) / 255.0, green: CGFloat((rgb & 0x00FF00) >> 8) / 255.0, blue: CGFloat(rgb & 0x0000FF) / 255.0, alpha: CGFloat(1.0))
    }
}

extension UIColor {
    enum App {
        static var blue: UIColor {
            return UIColor(rgb: 0x36B8C0)
        }
        
        static var lightBlue: UIColor {
            return UIColor(rgb: 0x56C4C3)
        }
        
        static var lightGray: UIColor {
            return UIColor(rgb: 0xEEEEEE)
        }
        
        static var mediumGray: UIColor {
            return UIColor(rgb: 0x3C3C3C)
        }
        
        static var darkGray: UIColor {
            return UIColor(rgb: 0x1F1F1F)
        }
    }
    
    static var tint: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return .white
                } else {
                    return .black
                }
            }
        } else {
            return .black
        }
    }
    
    static var invertedTint: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return .black
                } else {
                    return .white
                }
            }
        } else {
            return .white
        }
    }
    
    static var label: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return .lightGray
                } else {
                    return .darkGray
                }
            }
        } else {
            return .darkGray
        }
    }
    
    static var cell: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return App.darkGray
                } else {
                    return .white
                }
            }
        } else {
            return .white
        }
    }
    
    static var background: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return App.mediumGray
                } else {
                    return App.lightGray
                }
            }
        } else {
            return App.lightGray
        }
    }
    
    static var lightBackground: UIColor {
        if #available(iOS 13, *) {
            return UIColor { (UITraitCollection: UITraitCollection) -> UIColor in
                if UITraitCollection.userInterfaceStyle == .dark {
                    return App.mediumGray
                } else {
                    return .white
                }
            }
        } else {
            return .white
        }
    }

}
