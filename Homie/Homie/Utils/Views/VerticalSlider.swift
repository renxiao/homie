//
//  VerticalSlider.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

enum VerticalSliderType {
    case normal
    case heater
}

class VerticalSlider: UISlider {
    
    var type: VerticalSliderType = .normal {
        didSet {
            configureSliderType()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)

        transform = CGAffineTransform(rotationAngle: CGFloat(-(Double.pi / 2)))
        tintColor = UIColor.App.blue
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSliderType() {
        let stepsPerDegree: CGFloat = 2
        let minDegree: CGFloat = 7
        let maxDegree: CGFloat = 28
        
        let maxTemperature: CGFloat = (maxDegree - minDegree) * stepsPerDegree / 100
        
        switch type {
        case .heater:
            maximumValue = Float(maxTemperature)
        default:
            break
        }
    }
}
