//
//  OnOffButton.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

class OnOffButton: UIButton {
    
    var isOn: Bool = false {
        didSet {
            updateUI()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        let tintedImage = UIImage(named: "on_off")?.withRenderingMode(.alwaysTemplate)
        setImage(tintedImage, for: .normal)
        backgroundColor = .background
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func updateUI() {
        if isOn {
            backgroundColor = UIColor.App.lightBlue
            tintColor = .white
        } else {
            backgroundColor = .background
            tintColor = .lightGray
        }
    }
}
