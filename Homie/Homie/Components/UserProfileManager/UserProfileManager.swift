//
//  UserProfileManager.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxCocoa

protocol UserProfileProvider {
    var user: BehaviorRelay<User?> { get }
}

class UserProfileManager: UserProfileProvider {
    static let shared = UserProfileManager()

    var user = BehaviorRelay<User?>(value: nil)

    private init() {}
}
