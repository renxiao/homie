//
//  ResponseObject.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import Foundation

struct ResponseObject: Decodable {
    let devices: [DeviceObject]
    let user: User
}

struct DeviceObject: Decodable {
    let id: Int
    let deviceName: String
    let productType: String
    
    // Light
    let intensity: Int?
    
    // Heater
    let temperature: Int?
    
    // Light - Heater
    let mode: String?
    
    // RollerShutter
    let position: Int?
}

extension DeviceObject {
    var isOn: Bool {
        return mode == "ON" ? true : false
    }
}

extension DeviceObject {
    var toDevice: Device? {
        guard let type = DeviceType(rawValue: productType) else {
            return nil
        }
        
        switch type {
        case .light:
            return Light(id: id, name: deviceName, type: type, intensity: intensity ?? 0, isOn: isOn)
        case .heater:
            return Heater(id: id, name: deviceName, type: type, temperature: temperature ?? 18, isOn: isOn)
        case .rollerShutter:
            return RollerShutter(id: id, name: deviceName, type: type, position: position ?? 0)
        }
    }
}

struct User: Decodable {
    let firstName: String
    let lastName: String
    
    let address: UserAddress
    let birthDate: Int
}

extension User {
    struct UserAddress: Decodable {
        let city: String
        let postalCode: Int
        let street: String
        let streetCode: String
        let country: String
    }
}
