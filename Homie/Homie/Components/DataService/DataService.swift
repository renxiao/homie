//
//  DataService.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift

protocol DataProvider {
    func fetchData() -> Observable<ResponseObject>
}

class DataService: DataProvider {
    func fetchData() -> Observable<ResponseObject> {
        return APIClient().send(apiRequest: DataRequest())
    }
}
