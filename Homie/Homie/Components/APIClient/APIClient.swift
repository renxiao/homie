//
//  APIClient.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift

class APIClient {
    private let baseURL = URL(string: "http://storage42.com")

    func send<T: Decodable>(apiRequest: APIRequest) -> Observable<T> {
        guard let baseURL = baseURL else {
            return .empty()
        }
        return Observable<T>.create { observer in
            let request = apiRequest.request(with: baseURL)
            let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
                do {
                    let model: T = try JSONDecoder().decode(T.self, from: data ?? Data())
                    observer.onNext(model)
                } catch let error {
                    observer.onError(error)
                }
                observer.onCompleted()
            }
            task.resume()

            return Disposables.create {
                task.cancel()
            }
        }
    }
}
