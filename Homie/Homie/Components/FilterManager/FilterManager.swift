//
//  FilterManager.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxCocoa

protocol FilterProvider {
    var filters: BehaviorRelay<[Filter]> { get }
}

class FilterManager: FilterProvider {
    var filters = BehaviorRelay<[Filter]>(value: [
        Filter(type: .light, isEnabled: true),
        Filter(type: .heater, isEnabled: true),
        Filter(type: .rollerShutter, isEnabled: true)
    ])
    
    static let shared = FilterManager()
    
    private init() {}
}
