//
//  Device.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import Foundation

enum DeviceType: String {
    case light = "Light"
    case rollerShutter = "RollerShutter"
    case heater = "Heater"
    
    var localizedName: String {
        switch self {
        case .light:
            return "light".localized
        case .heater:
            return "heater".localized
        case .rollerShutter:
            return "roller_shutter".localized
        }
    }
}

extension DeviceType: Equatable {}

class Device {
    var id: Int
    var name: String
    var type: DeviceType
    
    init(id: Int, name: String, type: DeviceType) {
        self.id = id
        self.name = name
        self.type = type
    }
}

extension Device: Equatable {
    static func == (lhs: Device, rhs: Device) -> Bool {
        return lhs.id == rhs.id
            && lhs.name == rhs.name
            && lhs.type == rhs.type
    }
}

class Light: Device {
    var intensity: Int
    var isOn: Bool
    
    init(id: Int, name: String, type: DeviceType, intensity: Int, isOn: Bool) {
        self.intensity = intensity
        self.isOn = isOn
        super.init(id: id, name: name, type: type)
    }
}

class Heater: Device {
    var temperature: Int
    var isOn: Bool
    
    init(id: Int, name: String, type: DeviceType, temperature: Int, isOn: Bool) {
        self.temperature = temperature
        self.isOn = isOn
        super.init(id: id, name: name, type: type)
    }
}

extension Heater {
    static var minTemperature = 7
    static var maxTemperature = 28
}

class RollerShutter: Device {
    var position: Int
    
    init(id: Int, name: String, type: DeviceType, position: Int) {
        self.position = position
        super.init(id: id, name: name, type: type)
    }
}
