//
//  DevicesManager.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxCocoa

protocol DevicesProvider {
    var devices: BehaviorRelay<[Device]> { get }
}

class DevicesManager: DevicesProvider {
    static let shared = DevicesManager()
    let devices: BehaviorRelay<[Device]> = BehaviorRelay(value: [])
    
    private init() {}
}
