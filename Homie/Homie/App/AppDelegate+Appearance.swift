//
//  AppDelegate+Appearance.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

extension AppDelegate {
    func configureAppearance() {
        let tabBarAppearance = UITabBar.appearance()
        tabBarAppearance.backgroundImage = UIImage()
        tabBarAppearance.backgroundColor = .invertedTint
        tabBarAppearance.tintColor = .tint
        
        UINavigationBar.appearance().tintColor = .tint
        UINavigationBar.appearance().isTranslucent = false
    }
}
