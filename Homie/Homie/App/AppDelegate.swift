//
//  AppDelegate.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var appCoordinator: AppCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        let window = UIWindow()
        appCoordinator = AppCoordinator(window: window)
        appCoordinator?.start()

        configureAppearance()

        return true
    }
}
