//
//  AppCoordinator.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift

class AppCoordinator {
    private let window: UIWindow
    
    let disposeBag = DisposeBag()
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        
        let responseObject: Observable<ResponseObject> = DataService().fetchData()
        
        responseObject.subscribe(onNext: { responseObject in
            DevicesManager.shared.devices.accept(responseObject.devices.compactMap { $0.toDevice })
            UserProfileManager.shared.user.accept(responseObject.user)
        }, onError: { error in
            print(error.localizedDescription)
        }, onCompleted: {
            DispatchQueue.main.async {
                self.window.makeKeyAndVisible()
                self.window.rootViewController = TabBarController()
            }
        })
        .disposed(by: disposeBag)
    }
}
