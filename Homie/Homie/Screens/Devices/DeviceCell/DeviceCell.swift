//
//  DeviceCell.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift
import RxCocoa

class DeviceCell: UICollectionViewCell {
    
    static var cellIdentifier = "DeviceCell"
    
    let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        
        label.numberOfLines = 0
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    let onOffButton: OnOffButton = {
        let button = OnOffButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    var tapGesture: UITapGestureRecognizer?
    
    private var viewModel: DeviceCellViewModel!
    private let disposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: .zero)
        
        let onOffButtonWidth: CGFloat = 30
        onOffButton.anchor(size: CGSize(width: onOffButtonWidth, height: onOffButtonWidth))
        onOffButton.layer.cornerRadius = onOffButtonWidth / 2
        onOffButton.layer.masksToBounds = true
        
        let descriptionStackView = UIStackView(arrangedSubviews: [imageView, descriptionLabel])
        descriptionStackView.distribution = .equalSpacing
        descriptionStackView.spacing = 8
        
        let bottomStackView = UIStackView(arrangedSubviews: [descriptionStackView, onOffButton])
        bottomStackView.distribution = .equalSpacing
        
        let verticalStackView = UIStackView(arrangedSubviews: [nameLabel, bottomStackView])
        verticalStackView.axis = .vertical
        verticalStackView.distribution = .equalSpacing
        
        let padding: CGFloat = 12
        contentView.addSubview(verticalStackView)
        verticalStackView.fillSuperview(padding: .init(top: padding, left: padding, bottom: padding, right: padding))
  
        layer.cornerRadius = 8
        layer.masksToBounds = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func enableTapGesture(_ enabled: Bool) {
        
        if enabled {
            if tapGesture == nil {
                tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap))
                tapGesture?.numberOfTapsRequired = 1
            }
            guard let gesture = tapGesture else { return }
            addGestureRecognizer(gesture)
        } else {
            guard let tapGesture = tapGesture else { return }
            removeGestureRecognizer(tapGesture)
        }
    }

    @objc private func handleTap() {
        viewModel.isSelected.accept(!viewModel.isSelected.value)
    }
    
    func configure(with viewModel: DeviceCellViewModel) {
        self.viewModel = viewModel
        
        setupTapOnOffButtonHandling()
        updateUI()
    }
    
    private func setupTapOnOffButtonHandling() {
        onOffButton
            .rx
            .tap
            .subscribe { [weak self] _ in
                guard let self = self else { return }
                self.viewModel.saveMode(!self.onOffButton.isOn)
        }
        .disposed(by: disposeBag)
    }
    
    private func updateUI() {
        backgroundColor = viewModel.backgroundColor
        nameLabel.text = viewModel.name
        nameLabel.textColor = viewModel.textColor
        
        descriptionLabel.text = viewModel.description
        descriptionLabel.textColor = viewModel.textColor
        
        onOffButton.alpha = viewModel.onOffButtonConfig.isHidden ? 0 : 1
        onOffButton.isOn = viewModel.onOffButtonConfig.isOn
        
        let image = UIImage(named: viewModel.imageName)?.withRenderingMode(.alwaysTemplate)
        imageView.image = image
        imageView.tintColor = viewModel.imageTintColor
        
        contentView.alpha = viewModel.isSelected.value ? 1 : 0.3
    }
    
    func enableEditing(_ enabled: Bool) {
        onOffButton.isEnabled = !enabled
        enableTapGesture(enabled)

        if enabled {
            contentView.alpha = viewModel.isSelected.value ? 1 : 0.3
        } else {
            contentView.alpha = 1
        }
    }
}
