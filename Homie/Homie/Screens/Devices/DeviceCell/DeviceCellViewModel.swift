//
//  DeviceCellViewModel.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift
import RxCocoa

struct DeviceCellViewModel {

    private var device: Device
    private let devicesProvider: DevicesProvider
    
    let isOnBehaviorRelay = BehaviorRelay(value: false)
    
    let isSelected = BehaviorRelay(value: false)
    
    var name: String {
        return device.name
    }
    
    var description: String {
        switch device.type {
        case .light:
            return "\((device as? Light)?.intensity ?? 0)"
        case .heater:
            return "\((device as? Heater)?.temperature ?? 20)°"
        case .rollerShutter:
            return "\((device as? RollerShutter)?.position ?? 0)"
        }
    }
    
    var textColor: UIColor {
        switch device.type {
        case .light:
            guard let light = device as? Light else {
                return .darkGray
            }
            return light.isOn ? .white : .label
        case .heater:
            guard let heater = device as? Heater else {
                return .darkGray
            }
            return heater.isOn ? .white : .label
        case .rollerShutter:
            return .label
        }
    }
    
    var backgroundColor: UIColor {
        switch device.type {
        case .light:
            guard let light = device as? Light else {
                return .cell
            }
            return light.isOn ? UIColor.App.blue : .cell
        case .heater:
            guard let heater = device as? Heater else {
                return .cell
            }
            return heater.isOn ? UIColor.App.blue : .cell
        case .rollerShutter:
            return .cell
        }
    }
    
    var imageName: String {
        switch device.type {
        case .light:
            return "bulb"
        case .heater:
            return "thermometer"
        case .rollerShutter:
            return "roller_shutter_closed"
        }
    }
    
    var imageTintColor: UIColor {
        switch device.type {
        case .light:
            guard let light = device as? Light else {
                return .lightGray
            }
            return light.isOn ? .white : .lightGray
        case .heater:
            guard let heater = device as? Heater else {
                return .lightGray
            }
            return heater.isOn ? .white : .lightGray
        case .rollerShutter:
            return .lightGray
        }
    }
    
    var onOffButtonConfig: OnOffButtonConfig {
        return OnOffButtonConfig(for: device)
    }
    
    init(device: Device, isSelected: Bool, devicesProvider: DevicesProvider = DevicesManager.shared) {
        self.device = device
        self.devicesProvider = devicesProvider
        self.isSelected.accept(isSelected)
    }
    
    func saveMode(_ isOn: Bool) {
        isOnBehaviorRelay.accept(isOn)
        
        let devices = devicesProvider.devices.value
        guard let matchedDevice = devices.first(where: { $0 == device }) else { return }
        
        switch matchedDevice.type {
        case .light:
            guard let light = device as? Light else { return }
            light.isOn = isOn
            devicesProvider.devices.accept(devices)
        case .heater:
            guard let heater = device as? Heater else { return }
            heater.isOn = isOn
            devicesProvider.devices.accept(devices)
        case .rollerShutter:
            break
        }
    }
}

struct OnOffButtonConfig {
    private let device: Device
    
    var isHidden: Bool {
        switch device.type {
        case .light:
            return false
        case .heater:
            return false
        case .rollerShutter:
            return true
        }
    }
    
    var isOn: Bool {
        switch device.type {
        case .light:
            return (device as? Light)?.isOn ?? false
        case .heater:
            return (device as? Heater)?.isOn ?? false
        case .rollerShutter:
            return false
        }
    }
    
    init(for device: Device) {
        self.device = device
    }
}
