//
//  DevicesViewModel.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift
import RxCocoa

struct DevicesViewModel {
    
    private let devicesProvider: DevicesProvider
    private let filterProvider: FilterProvider
    
    var devices:  BehaviorRelay<[Device]> {
        return devicesProvider.devices
    }
    
    var isDeleteButtonEnabled: Observable<Bool> {
        return devicesToDelete.map {
            $0.count > 0
        }
    }
    
    var devicesToDelete = BehaviorRelay<[Device]>(value: [])
    
    private(set) var isEditing = BehaviorRelay<Bool>(value: false)
    
    var isDeleteButtonHidden: Observable<Bool> {
        return isEditing.map { !$0 }
    }
    
    init(devicesProvider: DevicesProvider = DevicesManager.shared,
        filterProvider: FilterProvider = FilterManager.shared) {
        self.devicesProvider = devicesProvider
        self.filterProvider = filterProvider
    }
    
    func reloadDevices() {
        let devices = devicesProvider.devices.value
        devicesProvider.devices.accept(devices)
    }
    
    func shouldShowDeviceType(_ type: DeviceType) -> Bool {
        guard let foundDevice = filterProvider.filters.value.first(where: { $0.type == type }) else { return false }
        return foundDevice.isEnabled
    }
    
    func toggleEditing() {
        isEditing.accept(!isEditing.value)
    }
    
    func addDeviceToDeletion(_ device: Device) {
        guard devicesToDelete.value.contains(device) == false else { return }
        var devices = devicesToDelete.value
        devices.append(device)
        devicesToDelete.accept(devices)
    }
    
    func removeDeviceFromDeletion(_ device: Device) {
        var devices = devicesToDelete.value
        guard let index = devices.firstIndex(of: device) else { return }
        devices.remove(at: index)
        devicesToDelete.accept(devices)
    }
    
    func executeDeletion() {
        var remainingDevices = devices.value
        remainingDevices.removeAll(where: { devicesToDelete.value.contains($0) })
        devices.accept(remainingDevices)
        devicesToDelete.accept([])
    }
}
