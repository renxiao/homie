//
//  DevicesViewController.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift

class DevicesViewController: UIViewController {
    private var viewModel = DevicesViewModel()
    private let disposeBag = DisposeBag()
    
    private let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20
        
        let screenWidth = UIScreen.main.bounds.width
        let margin: CGFloat = 20
        let itemWidth: CGFloat = screenWidth / 2 - (margin * 1.5)
        
        layout.sectionInset = UIEdgeInsets(top: margin, left: margin, bottom: margin, right: margin)
        layout.itemSize = CGSize(width: itemWidth, height: itemWidth / 3 * 2)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = UIColor.invertedTint.withAlphaComponent(0.9)
        collectionView.register(DeviceCell.self, forCellWithReuseIdentifier: DeviceCell.cellIdentifier)

        return collectionView
    }()
    
    private let deleteButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .lightBackground
        button.setTitleColor(.label, for: .normal)
        button.setTitleColor(UIColor.label.withAlphaComponent(0.3), for: .disabled)
        button.setTitle("delete".localized, for: .normal)
        button.titleLabel?.font = .systemFont(ofSize: 16, weight: .bold)
        button.addTarget(self, action: #selector(handleDelete), for: .touchUpInside)

        return button
    }()
}

// MARK: - View lifecycle

extension DevicesViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupFilterButtonInNavBar(filterSelector:  #selector(handleFilter), editSelector: #selector(handleEdit))
        setupSubviews()
        setupCellConfiguration()
        setupCellTapHandling()
        setupEditTapHandling()
    }
    
    private func setupView() {
        edgesForExtendedLayout = UIRectEdge(rawValue: 0)
        navigationItem.title = "devices".localized
    }
    
    private func setupSubviews() {
        let stackView = UIStackView(arrangedSubviews: [collectionView, deleteButton])
        stackView.axis = .vertical

        view.addSubview(stackView)
        deleteButton.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        deleteButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        stackView.fillSuperview()
    }
    
    
    @objc private func handleFilter() {
        let viewModel = FilterViewModel()
        let filterViewController = FilterViewController()
        filterViewController.viewModel = viewModel
        
        viewModel.filters
            .asObservable()
            .subscribe(onNext: { [weak self] _ in
                self?.viewModel.reloadDevices()
            })
            .disposed(by: disposeBag)
        
        present(UINavigationController(rootViewController: filterViewController), animated: true)
    }
    
    @objc private func handleEdit() {
        viewModel.toggleEditing()
    }
    
    @objc private func handleDelete() {
        viewModel.executeDeletion()
        viewModel.isEditing.accept(false)
    }
}

// MARK: - Rx setup

extension DevicesViewController {
    func setupCellConfiguration() {
        viewModel.devices
            .map({ $0.filter { [weak self] device in
                self?.viewModel.shouldShowDeviceType(device.type) ?? false
                }})
            .bind(to: collectionView
                .rx
                .items(cellIdentifier: DeviceCell.cellIdentifier)) { [weak self] index, device, cell in
                    guard let self = self else { return }
                    guard let cell = cell as? DeviceCell else { return }

                    let isDeviceSelected = self.viewModel.devicesToDelete.value.contains(device)
                    let cellViewModel = DeviceCellViewModel(device: device, isSelected: isDeviceSelected)

                    cell.configure(with: cellViewModel)

                    cellViewModel.isSelected.map { $0 }.subscribe(onNext: { selected in
                        cell.contentView.alpha = selected ? 1 : 0.3

                        if selected {
                            self.viewModel.addDeviceToDeletion(device)
                        } else {
                            self.viewModel.removeDeviceFromDeletion(device)
                        }
                    }).disposed(by: self.disposeBag)
                    
                    self.viewModel.isDeleteButtonHidden.subscribe(onNext: { isHidden in
                        cell.enableEditing(!isHidden)
                    }).disposed(by: self.disposeBag)
                    
        }.disposed(by: disposeBag)
    }
    
    func setupCellTapHandling() {
        collectionView
            .rx
            .modelSelected(Device.self)
            .subscribe(onNext: { [weak self] device in
                guard let self = self else { return }
                if self.viewModel.isEditing.value == false {
                    self.navigationController?.pushViewController(DeviceDetailCoordinator().viewController(viewModel: DeviceDetailViewModel(device: device)), animated: true)
                }
            })
            .disposed(by: disposeBag)
        
    }
    
    func setupEditTapHandling() {
        viewModel.isDeleteButtonHidden.bind(to: deleteButton.rx.isHidden).disposed(by: disposeBag)
        viewModel.isDeleteButtonEnabled.bind(to: deleteButton.rx.isEnabled).disposed(by: disposeBag)
    }
}
