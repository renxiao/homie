//
//  FilterCellViewModel.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import Foundation

struct FilterCellViewModel {
    
    private let filter: Filter
    
    var name: String {
        return filter.type.localizedName
    }

    var isEnabled: Bool {
        return filter.isEnabled
    }
    
    init(filter: Filter) {
        self.filter = filter
    }
}
