//
//  FilterViewController.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift
import RxCocoa

class FilterViewController: UIViewController {

    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.tableFooterView = UIView()
        tableView.register(FilterCell.self, forCellReuseIdentifier: FilterCell.cellIdentifier)

        return tableView
    }()

    var viewModel: FilterViewModel!
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .white
        preferredContentSize = CGSize(width: 200, height: 200)
        
        setupCloseButtonInNavBar(selector: #selector(handleDone))
        setupTableView()
        setupCellConfiguration()
        setupCellTapHandling()
    }
    
    private func setupTableView() {
        view.addSubview(tableView)
        tableView.fillSuperview()
        tableView.delegate = self
    }
    
    @objc private func handleDone() {
        dismiss(animated: true)
    }
}

// MARK: - Rx setup

extension FilterViewController {
    func setupCellConfiguration() {
        viewModel.filters
            .bind(to: tableView
                .rx
                .items(cellIdentifier: FilterCell.cellIdentifier)) { index, filter, cell in
                    let filterCellViewModel = FilterCellViewModel(filter: filter)

                    guard let cell = cell as? FilterCell else {
                        return
                    }

                    cell.configure(with: filterCellViewModel)
                    
        }.disposed(by: disposeBag)
    }
    
    func setupCellTapHandling() {
        tableView
            .rx
            .itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                self?.tableView.deselectRow(at: indexPath, animated: true)
                self?.viewModel.toggleFilterValue(atIndex: indexPath.row)
            })
            .disposed(by: disposeBag)
    }
}

extension FilterViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
}
