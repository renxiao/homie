//
//  FilterViewModel.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxCocoa

struct Filter {
    var type: DeviceType
    var isEnabled: Bool
}

class FilterViewModel {
    private let filterProvider: FilterProvider
    
    var filters: BehaviorRelay<[Filter]> {
        return filterProvider.filters
    }
    
    init(filterProvider: FilterProvider = FilterManager.shared) {
        self.filterProvider = filterProvider
    }
    
    func toggleFilterValue(atIndex index: Int) {
        guard index < filters.value.count else { return }
        var filterValues = filters.value
        
        filterValues[index].isEnabled = !filterValues[index].isEnabled
        
        filters.accept(filterValues)
    }
}
