//
//  UserProfileViewModel.swift
//  Homie
//
//  Created by Renxiao Mo on 02/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift
import RxCocoa

struct UserProfileViewModel {
    
    private let userProfileProvider: UserProfileProvider
    
    var user: User? {
        return userProfileProvider.user.value
    }
    
    var fullName = BehaviorRelay<String>(value: "")
    var birthDate = BehaviorRelay<String>(value: "")
    var fullAddress = BehaviorRelay<String>(value: "")

    
    init(userProfileProvider: UserProfileProvider = UserProfileManager.shared) {
        self.userProfileProvider = userProfileProvider
        
        updateBehaviorRelays()
    }
    
    func updateBehaviorRelays() {
        guard let user = user else { return }
        fullName.accept("\(user.firstName) \(user.lastName)")
        
        let date = Date(timeIntervalSince1970: Double(user.birthDate) / 1000)
        let formatter = DateFormatter.birthDateFormatter
        birthDate.accept(formatter.string(from: date))
        
        let address = user.address
        fullAddress.accept("\(address.streetCode) \(address.street)\n\(address.postalCode) \(address.city)\n\(address.country)")
        
    }
}
