//
//  UserProfileViewController.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift

class UserProfileViewController: UIViewController {

    let nameLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 24, weight: .bold)
        label.textColor = .label
        
        return label
    }()
    
    let birthDateLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 14, weight: .medium)
        label.textColor = .label
        return label
    }()
    
    let addressLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 18, weight: .medium)
        label.textColor = .label
        label.numberOfLines = 0
        
        return label
    }()
    
    let viewModel = UserProfileViewModel()
    private let disposeBag = DisposeBag()
}

// MARK: - View lifecycle

extension UserProfileViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupSubViews()
        setupEditButton()
        setupDataBinding()
    }
}

// MARK: - Basic setup

extension UserProfileViewController {
    private func setupView() {
        view.backgroundColor = .invertedTint
        edgesForExtendedLayout = UIRectEdge(rawValue: 0)

        navigationItem.title = "profile".localized
    }
    
    private func setupSubViews() {
        
        let topStackView = UIStackView(arrangedSubviews: [nameLabel, birthDateLabel])
        topStackView.axis = .vertical
        topStackView.spacing = 8
        
        let stackView = UIStackView(arrangedSubviews: [topStackView, addressLabel])
        stackView.axis = .vertical
        stackView.spacing = 20

        view.addSubview(stackView)
        stackView.anchor(centerX: view.centerXAnchor, centerY: view.centerYAnchor)
    }
    
    private func setupEditButton() {
        setupEditButtonInNavBar(selector: #selector(handleEdit))
    }
    
    @objc private func  handleEdit() {
        let editUserProfileViewController = EditUserProfileViewController()
        let editViewModel = EditUserProfileViewModel(user: viewModel.user)
        editUserProfileViewController.viewModel = editViewModel
        
        editUserProfileViewController.didEndEditing = { [weak self] in
            self?.viewModel.updateBehaviorRelays()
        }
        
        present(UINavigationController(rootViewController: editUserProfileViewController), animated: true)
    }
}

// MARK: - Rx setup

extension UserProfileViewController {
    private func setupDataBinding() {
        viewModel.fullName.bind(to: nameLabel.rx.text).disposed(by: disposeBag)
        viewModel.birthDate.bind(to: birthDateLabel.rx.text).disposed(by: disposeBag)
        viewModel.fullAddress.bind(to: addressLabel.rx.text).disposed(by: disposeBag)
    }
}
