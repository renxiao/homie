//
//  DeviceDetailCoordinator.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

class DeviceDetailCoordinator {
    func viewController(viewModel: DeviceDetailViewModel) -> DeviceDetailViewController {
        let viewController = DeviceDetailViewController()
        viewController.viewModel = viewModel
        
        return viewController
    }
}
