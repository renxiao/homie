//
//  DeviceDetailViewModel.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift
import RxCocoa

class DeviceDetailViewModel {

    private let device: Device
    private let devicesProvider: DevicesProvider
    
    let sliderValueBehaviorRelay = BehaviorRelay<Float>(value: 0)
    let isOnBehaviorRelay = BehaviorRelay(value: false)
    
    var isOnOffButtonHidden: Bool {
        return device.type == .rollerShutter
    }
    
    var isOn: Observable<Bool> {
        return isOnBehaviorRelay.asObservable()
    }
    
    var sliderValue: Observable<Float> {
        return sliderValueBehaviorRelay.map { value in
            return value
        }
    }
    
    var textValue: Observable<String> {
        return sliderValueBehaviorRelay.map { [weak self]  value in

            guard let self = self else { return  "0" }

            if case self.device.type = DeviceType.heater {
                return "\(self.calculateTemparature(fromSliderValue: value))"
            }

            return "\(Int(value * 100))"
        }
    }
    
    var startingSliderValue: Float {
        switch device.type {
        case .light:
            guard let light = device as? Light else { return 0 }
            return Float(light.intensity) / 100
        case .heater:
            guard let heater = device as? Heater else { return 0 }
            return calculateSliderValue(fromTemperature: heater.temperature)
        case .rollerShutter:
            guard let rollerShutter = device as? RollerShutter else { return 0 }
            return Float(rollerShutter.position) / 100
        }
    }
    
    private var startingOnOffValue: Bool {
        switch device.type {
        case .light:
            return (device as? Light)?.isOn ?? false
        case .heater:
            return (device as? Heater)?.isOn ?? false
        case .rollerShutter:
            return false
        }
    }
    
    var title: String {
        return device.name
    }
    
    var sliderType: VerticalSliderType {
        return device.type == .heater ? .heater : .normal
    }
    
    init(device: Device, devicesProvider: DevicesProvider = DevicesManager.shared) {
        self.device = device
        self.devicesProvider = devicesProvider
        
        isOnBehaviorRelay.accept(startingOnOffValue)
        sliderValueBehaviorRelay.accept(startingSliderValue)
    }
    
    private func calculateTemparature(fromSliderValue value: Float) -> Int {
        let stepsByDegree = Float(2)
        let minDegree = Float(7)

        return Int(value * 100 / stepsByDegree + minDegree)
    }
    
    private func calculateSliderValue(fromTemperature temperature: Int) -> Float {
        let stepsByDegree = 2
        let minDegree = 7
        
        return Float((temperature - minDegree) * stepsByDegree) / 100
    }
    
    func saveMode(_ isOn: Bool) {
        isOnBehaviorRelay.accept(isOn)
        
        let devices = devicesProvider.devices.value
        
        switch device.type {
        case .light:
            guard let light = device as? Light else { return }
            light.isOn = isOn
            devicesProvider.devices.accept(devices)
        case .heater:
            guard let heater = device as? Heater else { return }
            heater.isOn = isOn
            devicesProvider.devices.accept(devices)
        case .rollerShutter:
            break
        }
    }
    
    func saveValue(_ value: String) {
        let devices = devicesProvider.devices.value
        
        switch device.type {
        case .light:
            guard let light = device as? Light else { return }
            light.intensity = Int(value) ?? 0
            devicesProvider.devices.accept(devices)
        case .heater:
            guard let heater = device as? Heater else { return }
            heater.temperature = Int(value) ?? Heater.minTemperature
            devicesProvider.devices.accept(devices)
        case .rollerShutter:
            guard let rollerShutter = device as? RollerShutter else { return }
            rollerShutter.position = Int(value) ?? 0
            devicesProvider.devices.accept(devices)
        }
    }
}
