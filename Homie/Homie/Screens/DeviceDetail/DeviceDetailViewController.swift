//
//  DeviceDetailViewController.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift

class DeviceDetailViewController: UIViewController {
    
    var viewModel: DeviceDetailViewModel!
    
    let disposeBag = DisposeBag()
    
    let slider: VerticalSlider = {
        let slider = VerticalSlider()
        slider.translatesAutoresizingMaskIntoConstraints = false
        
        return slider
    }()
    
    let onOffButton: OnOffButton = {
        let button = OnOffButton()
        button.isOn = false
        button.translatesAutoresizingMaskIntoConstraints = false
        
        return button
    }()
    
    let valueLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 80)
        label.textColor = .label
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
}


// MARK: - View lifecycle

extension DeviceDetailViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupSlider()
        setupValueLabel()
        setupOnOffButton()
        setupSliderDidChangeHandling()
        setupOnOffState()
    }
}

// MARK: - Basic setup

extension DeviceDetailViewController {
    
    func setupView() {
        view.backgroundColor = .invertedTint
        title = viewModel.title
        navigationController?.navigationBar.isTranslucent = false
    }
    
    func setupValueLabel() {
        view.addSubview(valueLabel)
        valueLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        valueLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
    }
    
    func setupSlider() {
        view.addSubview(slider)
        slider.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.7).isActive = true
        slider.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: view.frame.width / 2 - 40).isActive = true
        slider.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        slider.type = viewModel.sliderType
        
        slider.value = viewModel.startingSliderValue
    }
    
    func setupOnOffButton() {
        view.addSubview(onOffButton)
        
        let width: CGFloat = 70
        let guide = view.safeAreaLayoutGuide

        onOffButton.anchor(bottom: guide.bottomAnchor, padding: .init(top: 0, left: 0, bottom: 20, right: 0), size: .init(width: width, height: width))
        onOffButton.anchor(centerX: view.centerXAnchor)
        
        onOffButton.layer.cornerRadius = width / 2
        onOffButton.layer.masksToBounds = true
        
        onOffButton.isHidden = viewModel.isOnOffButtonHidden
    }
}

// MARK: - Rx setup

extension DeviceDetailViewController {
    func setupSliderDidChangeHandling() {
        slider.rx.value.bind(to: viewModel.sliderValueBehaviorRelay).disposed(by: disposeBag)

        viewModel.sliderValue
            .bind(to: slider.rx.value)
            .disposed(by: disposeBag)
        
        viewModel.textValue
            .subscribe(onNext: { [weak self] value in
                self?.valueLabel.text = value
                self?.viewModel.saveValue(value)
            })
            .disposed(by: disposeBag)
    }
    
    func setupOnOffState() {
        viewModel.isOn
            .subscribe(onNext: { [weak self] isOn in
                self?.onOffButton.isOn = isOn
            })
            .disposed(by: disposeBag)
        
        onOffButton.rx.tap.subscribe { [weak self] _ in
            guard let self = self else { return }
            self.viewModel.saveMode(!self.onOffButton.isOn)
            
        }.disposed(by: disposeBag)
    }
}
