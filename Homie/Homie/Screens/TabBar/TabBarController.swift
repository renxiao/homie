//
//  TabBarController.swift
//  Homie
//
//  Created by Renxiao Mo on 01/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let devicesViewController = DevicesViewController()
        let devicesTabBarItem = UITabBarItem(title: "", image: UIImage(named: "tabbar_bulb.png"), selectedImage: UIImage(named: "tabbar_bulb.png"))
        devicesViewController.tabBarItem = devicesTabBarItem
        
        let profileViewController = UserProfileViewController()
        let profileTabBarItem = UITabBarItem(title: "", image: UIImage(named: "tabbar_profile.png"), selectedImage: UIImage(named: "tabbar_profile.png"))
        profileViewController.tabBarItem = profileTabBarItem
        
        let controllers = [devicesViewController, profileViewController]
        self.viewControllers = controllers.map { UINavigationController(rootViewController: $0) }
    }
}
