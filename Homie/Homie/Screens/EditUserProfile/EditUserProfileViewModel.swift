//
//  EditUserProfileViewModel.swift
//  Homie
//
//  Created by Renxiao Mo on 02/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift
import RxCocoa

class EditUserProfileViewModel {
    
    private let userProfileProvider: UserProfileProvider
    
    var firstName = BehaviorRelay<String>(value: "")
    var lastName = BehaviorRelay<String>(value: "")
    var birthdayDate = BehaviorRelay<String>(value: "")
    var streetCode = BehaviorRelay<String>(value: "")
    var street = BehaviorRelay<String>(value: "")
    var postalCode = BehaviorRelay<String>(value: "")
    var city = BehaviorRelay<String>(value: "")
    var country = BehaviorRelay<String>(value: "")
    
    var isBirthdayValid: Observable<Bool> {
        return birthdayDate.map { [weak self] birthdayDate in
            guard let self = self else { return false }
            return self.isBirthdayDateFormat(string: birthdayDate)
        }
    }
    
    init(user: User?, userProfileProvider: UserProfileProvider = UserProfileManager.shared) {
        
        self.userProfileProvider = userProfileProvider
        
        guard let user = user else { return }
        self.firstName.accept(user.firstName)
        self.lastName.accept(user.lastName)
        self.streetCode.accept(user.address.streetCode)
        self.street.accept(user.address.street)
        self.postalCode.accept("\(user.address.postalCode)")
        self.city.accept(user.address.city)
        self.country.accept(user.address.country)

        let date = Date(timeIntervalSince1970: Double(user.birthDate) / 1000)
        let formatter = DateFormatter.birthDateFormatter
        self.birthdayDate.accept(formatter.string(from: date))
    }
    
    private func isBirthdayDateFormat(string: String) -> Bool {
        let pattern = "^([0-2][0-9]||3[0-1])/(0[0-9]||1[0-2])/[0-9][0-9][0-9][0-9]$"
        let regex = try? NSRegularExpression(pattern: pattern)
        let range = NSRange(location: 0, length: string.utf16.count)
        return regex?.firstMatch(in: string, options: [], range: range) != nil
    }
    
    func save() {
        let user = User(firstName: firstName.value,
                        lastName: lastName.value,
                        address: User.UserAddress(city: city.value,
                                                  postalCode: Int(postalCode.value) ?? 0,
                                                  street: street.value,
                                                  streetCode: streetCode.value,
                                                  country: country.value),
                        birthDate: Int(birthdayTimeStamp()))
        userProfileProvider.user.accept(user)
    }
    
    private func birthdayTimeStamp() -> Double {
        return (DateFormatter.birthDateFormatter.date(from: birthdayDate.value)?.timeIntervalSince1970 ?? 0) * 1000
    }
}
