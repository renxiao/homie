//
//  EditUserProfileViewController.swift
//  Homie
//
//  Created by Renxiao Mo on 02/03/2021.
//  Copyright © 2021 Renxiao Mo. All rights reserved.
//

import RxSwift

class EditUserProfileViewController: UIViewController {
    
    let firstNameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "first_name".localized
        
        return textField
    }()
    
    let lastNameTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "last_name".localized
        
        return textField
    }()
    
    let birthdayDateTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "birthday_date".localized
        
        return textField
    }()
    
    let birthdayWrongFormatLabel: UILabel = {
        let label = UILabel()
        label.font = .systemFont(ofSize: 12)
        label.textColor = .red
        label.text = "dd/mm/yyyy"
        return label
    }()
    
    let streetCodeTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "street_code".localized
        
        return textField
    }()
    
    let streetTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "street".localized
        
        return textField
    }()
    
    let postalCodeTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "postal_code".localized
        
        return textField
    }()
    
    let cityTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "city".localized
        
        return textField
    }()
    
    let countryTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "country".localized
        
        return textField
    }()
    
    var viewModel: EditUserProfileViewModel!
    private let disposeBag = DisposeBag()
    
    var didEndEditing: (() -> Void)?
}

// MARK: - View lifecycle

extension EditUserProfileViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .invertedTint
        
        setupSubviews()
        setupNavigationBarButtons()
        setupReturnKeyPressHandling()
        setupTextFieldInitialValues()
        setupDataBinding()
    }
}

// MARK: - Basic setup

extension EditUserProfileViewController {
    
    private func setupSubviews() {
        let nameStackView = UIStackView(arrangedSubviews: [firstNameTextField, lastNameTextField])
        nameStackView.distribution = .fillEqually
        
        let birthdayStackView = UIStackView(arrangedSubviews: [birthdayDateTextField, birthdayWrongFormatLabel])
        birthdayStackView.distribution = .fillEqually
        
        let streetStackView = UIStackView(arrangedSubviews: [streetCodeTextField, streetTextField])
        streetStackView.distribution = .fillEqually
        
        
        let cityStackView = UIStackView(arrangedSubviews: [postalCodeTextField, cityTextField])
        cityStackView.distribution = .fillEqually
        
        
        let stackView = UIStackView(arrangedSubviews: [
            nameStackView,
            birthdayStackView,
            streetStackView,
            cityStackView,
            countryTextField
        ])
        stackView.axis = .vertical
        stackView.spacing = 20
        
        let padding: CGFloat = 20
        view.addSubview(stackView)
        stackView.anchor(top: view.topAnchor,
                         leading: view.leadingAnchor,
                         trailing: view.trailingAnchor,
                         padding: .init(top: padding, left: padding, bottom: padding, right: padding))
    }
    
    private func setupNavigationBarButtons() {
        setupCancelButtonInNavBar(selector: #selector(handleCancel))
        let doneButtonItem = setupDoneButtonInNavBar(selector: #selector(handleDone))
        
        viewModel.isBirthdayValid.bind(to: doneButtonItem.rx.isEnabled).disposed(by: disposeBag)
    }
    
    @objc private func handleCancel() {
        dismiss(animated: true)
    }
    
    @objc private func handleDone() {
        viewModel.save()
        didEndEditing?()
        dismiss(animated: true)
    }
}

// MARK: - Rx setup

extension EditUserProfileViewController {
    func setupReturnKeyPressHandling() {
        
        let textFields = [firstNameTextField,
                          lastNameTextField,
                          birthdayDateTextField,
                          streetCodeTextField,
                          streetTextField,
                          postalCodeTextField,
                          cityTextField,
                          countryTextField]
        
        textFields.enumerated().forEach { index, textField in
            if index == textFields.count - 1 {
                textField.rx.controlEvent([.editingDidEndOnExit]).subscribe { _ in
                    textField.resignFirstResponder()
                }.disposed(by: disposeBag)
            } else {
                textField.rx.controlEvent([.editingDidEndOnExit]).subscribe { _ in
                    let nextTextField = textFields[index + 1]
                    nextTextField.becomeFirstResponder()
                }.disposed(by: disposeBag)
            }
            
        }
    }
    
    func setupTextFieldInitialValues() {
        firstNameTextField.text = viewModel.firstName.value
        lastNameTextField.text = viewModel.lastName.value
        birthdayDateTextField.text = viewModel.birthdayDate.value
        streetCodeTextField.text = viewModel.streetCode.value
        streetTextField.text = viewModel.street.value
        postalCodeTextField.text = viewModel.postalCode.value
        cityTextField.text = viewModel.city.value
        countryTextField.text = viewModel.country.value
    }
    
    func setupDataBinding() {
        firstNameTextField.rx.text
            .orEmpty
            .bind(to: viewModel.firstName)
            .disposed(by: disposeBag)
        
        lastNameTextField.rx.text
            .orEmpty
            .bind(to: viewModel.lastName)
            .disposed(by: disposeBag)

        birthdayDateTextField.rx.text
            .orEmpty
            .bind(to: viewModel.birthdayDate)
            .disposed(by: disposeBag)
        
        streetCodeTextField.rx.text
            .orEmpty
            .bind(to: viewModel.streetCode)
            .disposed(by: disposeBag)
        
        streetTextField.rx.text
            .orEmpty
            .bind(to: viewModel.street)
            .disposed(by: disposeBag)
        
        postalCodeTextField.rx.text
            .orEmpty
            .bind(to: viewModel.postalCode)
            .disposed(by: disposeBag)
        
        cityTextField.rx.text
            .orEmpty
            .bind(to: viewModel.city)
            .disposed(by: disposeBag)
        
        countryTextField.rx.text
            .orEmpty
            .bind(to: viewModel.country)
            .disposed(by: disposeBag)
        
        viewModel.isBirthdayValid.bind(to: birthdayWrongFormatLabel.rx.isHidden).disposed(by: disposeBag)
    }
}
